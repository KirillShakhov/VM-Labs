package lab.models.lab3;


import lab.models.lab2.Point;

import java.util.ArrayList;

public class ResultSetForIntegral {
    private Double result;
    private Double steps;
    private Double eps;

    public Double getResult() {
        return result;
    }
    public void setResult(Double result) {
        this.result = result;
    }
    public Double getSteps() {
        return steps;
    }
    public void setSteps(Double steps) {
        this.steps = steps;
    }
    public Double getEps() {
        return eps;
    }
    public void setEps(Double eps) {
        this.eps = eps;
    }
}
